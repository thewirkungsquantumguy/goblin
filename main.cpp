#include <iostream>
#include <string>
 
typedef enum
{
               HAPPY,
               ANGRY,
               NEUTRAL
}MOOD_t;
 
typedef struct
{
               int healthLvl;
               int expLvl;
               bool hasWeapon;
               MOOD_t mood;
}goblin_t;
 
bool isHappy(goblin_t monster);
void assignWeapon(goblin_t* monster);
void drinkHealthPotion(goblin_t* monster);
void printMood(goblin_t monster);
void printWeaponStatus(goblin_t monster);
void initGoblin(goblin_t* monster, int healthLvl, int expLvl, bool hasWeapon, MOOD_t mood);
void buildArmy(goblin_t* army, size_t sizeOfArmy);
void dealWithThomas(goblin_t* thomas);
void pointersThingy();
 
int main()
{
    static const int sizeOfArmy = 1000;
    goblin_t army[sizeOfArmy];
    buildArmy(army, sizeof(army));
   
               goblin_t thomas;
               dealWithThomas(&thomas);
 
    pointersThingy();
 
    int foo;
    std::cin >> foo;
               return 0;
}
 
void pointersThingy()
{
    std::cout << "\n" << "Let's remember the pointers thing..." << "\n";
    int NumberOfLives = 7;
    int* pNumberOfLives = &NumberOfLives;
   
    std::cout << NumberOfLives << "\n";
    std::cout << pNumberOfLives << "\n";
    std::cout << *pNumberOfLives << "\n";
    std::cout << &pNumberOfLives << "\n";
}
 
void buildArmy(goblin_t* army, size_t sizeOfArmy)
{
    for(int i = 0; i <= (int)sizeOfArmy; i++)
    {
        bool hasWeapon = false;
        MOOD_t mood = HAPPY;
               
        if(i % 5 == 0)
        {
            hasWeapon = true;
        }
       
        if(i % 3 == 0)
        {
            mood = ANGRY;
        }
       
        initGoblin(&army[i], 50, 10, hasWeapon, mood);
    }
 
    int armed=0;
    int angry=0;
    for(int i = 0; i <= (int)sizeOfArmy; i++)
    {
        if(army[i].hasWeapon)
        {
            armed++;
        }
 
        if (army[i].mood == ANGRY)
        {
            angry++;
        }
    }
}
 
void dealWithThomas(goblin_t* thomas)
{
    initGoblin(thomas, 50, 10, false, ANGRY);
              
               if(!isHappy(*thomas))
               {
                              assignWeapon(thomas);
               }
              
               drinkHealthPotion(thomas);
              
               std::cout << thomas->healthLvl << "\n";
               std::cout << thomas->expLvl << "\n";
               printWeaponStatus(*thomas);
               printMood(*thomas);
}
 
void initGoblin(goblin_t* monster, int healthLvl, int expLvl, bool hasWeapon, MOOD_t mood)
{
    monster->healthLvl = healthLvl;
               monster->expLvl = expLvl;
               monster->hasWeapon = hasWeapon;
               monster->mood = mood;
}
 
bool isHappy(goblin_t monster)
{
               if(monster.mood == HAPPY)
               {
                              return true;
               }
               else
               {
                              return false;
               }
}
 
void drinkHealthPotion(goblin_t* monster)
{
               monster->healthLvl += 70;
              
               if(monster->healthLvl > 100)
               {
                              monster->healthLvl = 100;
               }
}
 
void printMood(goblin_t monster)
{
    switch(monster.mood)
    {
        case HAPPY:
        {
            std::cout << "HAPPY" << "\n";
            break;
        }
        case ANGRY:
        {
            std::cout << "ANGRY" << "\n";
            break;
        }
        case NEUTRAL:
        {
            std::cout << "NEUTRAL" << "\n";
            break;
        }
    }   
}
void printWeaponStatus(goblin_t monster)
{
    if(monster.hasWeapon)
               {
                   std::cout << "He has a pair of tweasers" << "\n";
               }
               else
               {
                   std::cout << "No weapon (kick his ass)" << "\n";
               }
}
void assignWeapon(goblin_t* monster)
{
               monster->hasWeapon = true;
}